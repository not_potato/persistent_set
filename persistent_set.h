#include <type_traits>
#include <iterator>
#include <utility>
#include <cstddef>
#include <random>

#include <iostream>

namespace {
auto generator = std::mt19937{};
}

namespace bezborodov::smart_ptrs {

template <typename T>
class shared_ptr {
public:
    constexpr shared_ptr() noexcept;
    constexpr shared_ptr(std::nullptr_t) noexcept;
    explicit shared_ptr(T* ptr);
    shared_ptr(shared_ptr const& rhs) noexcept;
    shared_ptr(shared_ptr&& rhs) noexcept;
    shared_ptr& operator=(shared_ptr const& rhs) noexcept;
    shared_ptr& operator=(shared_ptr&& rhs) noexcept;
    ~shared_ptr() noexcept;

    void reset() noexcept;
    void reset(T* ptr);
    void swap(shared_ptr& rhs) noexcept;
    T* get() const noexcept;
    T& operator*() const noexcept;
    T* operator->() const noexcept;
    long count() const noexcept;
    explicit operator bool() const noexcept;

private:
    // m_ptr == nullptr <=> m_count == nullptr
    T* m_ptr = nullptr;
    long* m_count = nullptr;
};

template <typename T>
class linked_ptr {
public:
    constexpr linked_ptr() noexcept;
    constexpr linked_ptr(std::nullptr_t) noexcept;
    explicit linked_ptr(T* ptr) noexcept;
    linked_ptr(linked_ptr const& rhs) noexcept;
    linked_ptr(linked_ptr&& rhs) noexcept;
    linked_ptr& operator=(linked_ptr const& rhs) noexcept;
    linked_ptr& operator=(linked_ptr&& rhs) noexcept;
    ~linked_ptr() noexcept;

    void reset(T* ptr = nullptr) noexcept;
    void swap(linked_ptr& rhs) noexcept;
    T* get() const noexcept;
    T& operator*() const noexcept;
    T* operator->() const noexcept;
    long count() const noexcept;
    explicit operator bool() const noexcept;

private:
    // m_ptr == nullptr <=> (m_left == nullptr && m_right == nullptr)
    T* m_ptr = nullptr;
    mutable linked_ptr const* m_left = nullptr;
    mutable linked_ptr const* m_right = nullptr;

    // ensure that m_left->right == this && m_right->left == this
    void luke_i_am_your_father() const noexcept;
};

} // bezborodov::smart_ptrs namespace

namespace bezborodov {

template <typename T, template <typename> class U = smart_ptrs::shared_ptr>
class persistent_set {
public:
    template <typename X>
    using smart_ptr = U<X>;
    using value_type = T;

    struct iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;

    constexpr persistent_set() noexcept = default;
    persistent_set(persistent_set const& rhs) noexcept = default;
    persistent_set(persistent_set&& rhs) noexcept = default;
    persistent_set& operator=(persistent_set const& rhs) noexcept = default;
    persistent_set& operator=(persistent_set&& rhs) noexcept = default;
    ~persistent_set() noexcept = default;

    iterator begin() noexcept;
    iterator end() noexcept;
    iterator cbegin() const noexcept;
    iterator cend() const noexcept;
    reverse_iterator rbegin() noexcept;
    reverse_iterator rend() noexcept;
    reverse_iterator crbegin() const noexcept;
    reverse_iterator crend() const noexcept;

    bool empty() const noexcept;
    size_t size() const noexcept;

    std::pair<iterator, bool> insert(value_type const& value);
    
    template <typename = std::enable_if_t<std::is_move_constructible_v<value_type>>>
    std::pair<iterator, bool> insert(value_type&& value);
    void erase(iterator it);
    iterator find(value_type const& val) const;

private:
    struct node_base;
    struct node;

public:
    struct iterator {
        using difference_type = std::ptrdiff_t;
        using value_type = persistent_set<T, U>::value_type;
        using pointer = value_type const*;
        using reference = value_type&;
        using iterator_category = std::bidirectional_iterator_tag;

        constexpr iterator() noexcept = default;
        iterator(iterator const& rhs) noexcept = default;
        iterator(iterator&& rhs) noexcept = default;
        iterator& operator=(iterator const& rhs) noexcept = default;
        iterator& operator=(iterator&& rhs) noexcept = default;

        value_type const& operator*() const noexcept;

        iterator& operator++() noexcept;
        iterator operator++(int) noexcept;
        iterator& operator--() noexcept;
        iterator operator--(int) noexcept;

        bool operator==(iterator const&) noexcept;
        bool operator!=(iterator const&) noexcept;

    private:
        node_base const* _fake_root;
        node_base const* m_node;

        iterator(node_base const* _fake_root, node_base const* m_node) noexcept;

        friend class persistent_set;
    };

private:
    node_base _fake_root{}; // root
    size_t _size = 0;

    struct node_base {
        smart_ptr<node> m_left;
        constexpr node_base() noexcept = default;
        node_base(node_base const&) noexcept = default;
        node_base& operator=(node_base const&) noexcept = default;
        node_base(node_base&& rhs) = default;
        node_base& operator=(node_base&&) noexcept = default;
    };

    struct node : public node_base {
        smart_ptr<node> m_right;
        value_type m_data;
        typename decltype(::generator)::result_type m_prior;

        explicit node(value_type const& value);
        
        template <typename = std::enable_if<std::is_move_constructible_v<T>>>
        explicit node(value_type&& value);

        node() = delete;
        node(node const&) = default;
        node& operator=(node const&) = default;
    };

    template <typename T1, template <typename> class U1>
    friend inline void split(U1<typename persistent_set<T1, U1>::node> const& root,
                      U1<typename persistent_set<T1, U1>::node>& l,
                      U1<typename persistent_set<T1, U1>::node>& r,
                      T1 const& value,
                      bool equalToLeftTree);

    template <typename T1, template <typename> class U1>
    friend inline U1<typename persistent_set<T1, U1>::node>
                    merge(U1<typename persistent_set<T1, U1>::node> const& l,
                          U1<typename persistent_set<T1, U1>::node> const& r);

    template <typename T1, template <typename> class U1>
    friend inline U1<typename persistent_set<T1, U1>::node>
                    insert(U1<typename persistent_set<T1, U1>::node> const& t,
                           U1<typename persistent_set<T1, U1>::node>& newNode);

    template <typename T1, template <typename> class U1>
    friend inline U1<typename persistent_set<T1, U1>::node>
                    erase(U1<typename persistent_set<T1, U1>::node> const& t,
                          T1 const& value);

    template <typename T1, template <typename> class U1>
    friend inline U1<typename persistent_set<T1, U1>::node>
            find(U1<typename persistent_set<T1, U1>::node> const& t,
                 T1 const& value);
};

} // bezborodov namespace

namespace bezborodov::smart_ptrs {

template <typename T>
inline constexpr shared_ptr<T>::shared_ptr() noexcept
{
}

template <typename T>
inline constexpr shared_ptr<T>::shared_ptr(std::nullptr_t) noexcept
{
}

template <typename T>
inline shared_ptr<T>::shared_ptr(T* ptr)
    : m_ptr(ptr)
    , m_count(nullptr)
{
    if (m_ptr) {
        m_count = new long{ 1 };
    }
}

template <typename T>
inline shared_ptr<T>::shared_ptr(shared_ptr const& rhs) noexcept
    : m_ptr(rhs.m_ptr)
    , m_count(rhs.m_count)
{
    if (m_count) {
        ++*m_count;
    }
}

template <typename T>
inline shared_ptr<T>::shared_ptr(shared_ptr&& rhs) noexcept
{
    swap(rhs);
}

template <typename T>
inline shared_ptr<T>& shared_ptr<T>::operator=(shared_ptr const& rhs) noexcept
{
    shared_ptr<T> temp(rhs);
    swap(temp);
    return *this;
}

template <typename T>
inline shared_ptr<T>& shared_ptr<T>::operator=(shared_ptr&& rhs) noexcept
{
    swap(rhs);
    return *this;
}

template <typename T>
inline shared_ptr<T>::~shared_ptr<T>() noexcept
{
    if (m_count && --*m_count == 0) {
        delete m_ptr;
        delete m_count;
    }
}

template <typename T>
inline void shared_ptr<T>::reset() noexcept
{
    shared_ptr<T> temp;
    swap(temp);
}

template <typename T>
inline void shared_ptr<T>::reset(T* ptr)
{
    shared_ptr<T> temp(ptr);
    swap(temp);
}

template <typename T>
inline void shared_ptr<T>::swap(shared_ptr& rhs) noexcept
{
    std::swap(m_ptr, rhs.m_ptr);
    std::swap(m_count, rhs.m_count);
}

template <typename T>
inline T* shared_ptr<T>::get() const noexcept
{
    return m_ptr;
}

template <typename T>
inline T& shared_ptr<T>::operator*() const noexcept
{
    return *m_ptr;
}

template <typename T>
inline T* shared_ptr<T>::operator->() const noexcept
{
    return m_ptr;
}

template <typename T>
inline long shared_ptr<T>::count() const noexcept
{
    return m_count ? *m_count : 0;
}

template <typename T>
inline shared_ptr<T>::operator bool() const noexcept
{
    return m_ptr;
}

template <typename T>
inline constexpr linked_ptr<T>::linked_ptr() noexcept
{
}

template <typename T>
inline constexpr linked_ptr<T>::linked_ptr(std::nullptr_t) noexcept
{
}

template <typename T>
inline linked_ptr<T>::linked_ptr(T* ptr) noexcept
    : m_ptr(ptr)
    , m_left(nullptr)
    , m_right(nullptr)
{
}

template <typename T>
inline linked_ptr<T>::linked_ptr(linked_ptr const& rhs) noexcept
    : m_ptr(rhs.m_ptr)
    , m_left(rhs.m_left)
    , m_right(rhs.m_ptr ? &rhs : nullptr)
{
    if (rhs.m_ptr) {
        if (rhs.m_left) {
            rhs.m_left->m_right = this;
        }
        rhs.m_left = this;
    }
}

template <typename T>
inline linked_ptr<T>::linked_ptr(linked_ptr&& rhs) noexcept
{
    swap(rhs);
}

template <typename T>
inline linked_ptr<T>& linked_ptr<T>::operator=(linked_ptr const& rhs) noexcept
{
    linked_ptr<T> temp(rhs);
    swap(temp);
    return *this;
}

template <typename T>
inline linked_ptr<T>& linked_ptr<T>::operator=(linked_ptr&& rhs) noexcept
{
    swap(rhs);
    return *this;
}

template <typename T>
inline linked_ptr<T>::~linked_ptr<T>() noexcept
{
    if (m_left == m_right) {
        delete m_ptr;
        return;
    }
    if (m_left) {
        m_left->m_right = m_right;
    }
    if (m_right) {
        m_right->m_left = m_left;
    }
}

template <typename T>
inline void linked_ptr<T>::reset(T* ptr) noexcept
{
    linked_ptr<T> temp(ptr);
    swap(temp);
}

template <typename T>
inline void linked_ptr<T>::swap(linked_ptr& rhs) noexcept
{
    if (m_ptr != rhs.m_ptr) {
        std::swap(m_left, rhs.m_left);
        std::swap(m_right, rhs.m_right);
        std::swap(m_ptr, rhs.m_ptr);
        luke_i_am_your_father();
        rhs.luke_i_am_your_father();
    }
}

template <typename T>
inline T* linked_ptr<T>::get() const noexcept
{
    return m_ptr;
}

template <typename T>
inline T& linked_ptr<T>::operator*() const noexcept
{
    return *m_ptr;
}

template <typename T>
inline T* linked_ptr<T>::operator->() const noexcept
{
    return m_ptr;
}

template <typename T>
inline long linked_ptr<T>::count() const noexcept
{
    if (!m_ptr) {
        return 0;
    }
    long ans = 1;
    for (linked_ptr<T> const* cur = m_left; cur; cur = cur->m_left, ++ans) {
    }
    for (linked_ptr<T> const* cur = m_right; cur; cur = cur->m_right, ++ans) {
    }
    return ans;
}

template <typename T>
inline linked_ptr<T>::operator bool() const noexcept
{
    return m_ptr;
}

template <typename T>
inline void linked_ptr<T>::luke_i_am_your_father() const noexcept
{
    if (m_left) {
        m_left->m_right = this;
    }
    if (m_right) {
        m_right->m_left = this;
    }
}

} // bezborodov::smart_ptrs namespace

namespace bezborodov {
template <typename T, template <typename> class U>
inline void split(U<typename persistent_set<T, U>::node> const& root,
    U<typename persistent_set<T, U>::node>& l,
    U<typename persistent_set<T, U>::node>& r,
    T const& value,
    bool equalToLeftTree = true)
{
    if (!root) {
        l = r = nullptr;
        return;
    }
    U<typename persistent_set<T, U>::node> cur(new typename persistent_set<T, U>::node(*root.get()));
    if (value < cur->m_data) {
        split(cur->m_left, l, cur->m_left, value, equalToLeftTree);
        r = cur;
    } else if (!(value == cur->m_data)) {
        split(cur->m_right, cur->m_right, r, value, equalToLeftTree);
        l = cur;
    } else if (equalToLeftTree) {
        r = cur->m_right;
        cur->m_right = nullptr;
        l = cur;
    } else {
        l = cur->m_left;
        cur->m_left = nullptr;
        r = cur;
    }
}

template <typename T, template <typename> class U>
[[nodiscard]] inline U<typename persistent_set<T, U>::node> 
                merge(U<typename persistent_set<T, U>::node> const& l,
                      U<typename persistent_set<T, U>::node> const& r)
{

    using ptr_t = U<typename persistent_set<T, U>::node>;
    if (!l && !r) {
        ptr_t cur;
        return std::move(cur);
    }
    if (!l || !r) {
        ptr_t cur(new typename persistent_set<T, U>::node(l ? *l : *r));
        return std::move(cur);
    }
    if (l->m_prior > r->m_prior) {
        ptr_t cur(new typename persistent_set<T, U>::node(*l));
        cur->m_right = std::move(merge<T, U>(cur->m_right, r));
        return std::move(cur);
    } else {
        ptr_t cur(new typename persistent_set<T, U>::node(*r));
        cur->m_left = std::move(merge<T, U>(l, cur->m_left));
        return std::move(cur);
    }
}

template <typename T, template <typename> class U>
[[nodiscard]] inline U<typename persistent_set<T, U>::node>
                insert(U<typename persistent_set<T, U>::node> const& t,
                       U<typename persistent_set<T, U>::node>& newNode)
{
    using node_t = typename persistent_set<T, U>::node;
    using ptr_t = U<node_t>;
    if (!t) {
        return newNode;
    } else if (newNode->m_prior > t->m_prior) {
        ::bezborodov::split<T, U>(t, newNode->m_left, newNode->m_right, newNode->m_data);
        return std::move(newNode);
    } else {
        ptr_t cur(new node_t(*t));
        auto& x = newNode->m_data < cur->m_data ? cur->m_left : cur->m_right;
        x = std::move(::bezborodov::insert<T, U>(x, newNode));
        return std::move(cur);
    }
}

template <typename T, template <typename> class U>
[[nodiscard]] inline U<typename persistent_set<T, U>::node> 
                erase(U<typename persistent_set<T, U>::node> const& t,
                      T const& value)
{
    using node_t = typename persistent_set<T, U>::node;
    using ptr_t = U<node_t>;
    if (t->m_data == value) {
        return std::move(::bezborodov::merge<T, U>(t->m_left, t->m_right));
    } else {
        ptr_t cur(new node_t(*t));
        auto& x = value < cur->m_data ? cur->m_left : cur->m_right;
        x = std::move(::bezborodov::erase<T, U>(x, value));
        return std::move(cur);
    }
}

template <typename T, template <typename> class U>
inline U<typename persistent_set<T, U>::node>
            find(U<typename persistent_set<T, U>::node> const& t,
                 T const& value)
{
    U<typename persistent_set<T, U>::node> res(t);
    while (res && !(res->m_data == value))
    {
        res = value < res->m_data ? res->m_left : res->m_right;
    }
    return std::move(res);
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator persistent_set<T, U>::begin() noexcept
{
    return std::move(cbegin());
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator persistent_set<T, U>::end() noexcept
{
    return std::move(cend());
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator persistent_set<T, U>::cbegin() const noexcept
{
    node_base const* cur = &_fake_root;
    for (; cur && cur->m_left; cur = cur->m_left.get()) {
    }
    return std::move(iterator(&_fake_root, cur));
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator persistent_set<T, U>::cend() const noexcept
{
    return std::move(iterator{ &_fake_root, &_fake_root });
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::reverse_iterator persistent_set<T, U>::rbegin() noexcept
{
    return std::move(reverse_iterator{ end() });
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::reverse_iterator persistent_set<T, U>::rend() noexcept
{
    return std::move(reverse_iterator{ begin() });
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::reverse_iterator persistent_set<T, U>::crbegin() const noexcept
{
    return std::move(reverse_iterator{ cend() });
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::reverse_iterator persistent_set<T, U>::crend() const noexcept
{
    return std::move(reverse_iterator{ cbegin() });
}

template <typename T, template <typename> class U>
inline bool persistent_set<T, U>::empty() const noexcept
{
    return _size == 0;
}

template <typename T, template <typename> class U>
inline size_t persistent_set<T, U>::size() const noexcept
{
    return _size;
}

template <typename T, template <typename> class U>
inline std::pair<typename persistent_set<T, U>::iterator, bool> persistent_set<T, U>::insert(value_type const& value)
{
    if (auto it = find(value); it != end()) {
        return std::move(decltype(insert(value)){ it, false });
    }
    ++_size;
    smart_ptr<node> newNode(new node(value));
    _fake_root.m_left = std::move(::bezborodov::insert<T, U>(_fake_root.m_left, newNode));
    return std::move(decltype(insert(value)){ {&_fake_root, newNode.get() }, true });
}

template <typename T, template <typename> class U>
template <typename>
inline std::pair<typename persistent_set<T, U>::iterator, bool> persistent_set<T, U>::insert(value_type&& value)
{
    if (auto it = find(value); it != end()) {
        return std::move(decltype(insert(value)){ it, false });
    }
    ++_size;
    smart_ptr<node> newNode(new node(std::move(value)));
    _fake_root.m_left = std::move(::bezborodov::insert<T, U>(_fake_root.m_left, newNode));
    return std::move(decltype(insert(value)){ {&_fake_root, newNode.get() }, true });
}

template <typename T, template <typename> class U>
inline void persistent_set<T, U>::erase(typename persistent_set<T, U>::iterator it)
{
    --_size;
    _fake_root.m_left = std::move(::bezborodov::erase<T, U>
                                                (_fake_root.m_left,
                                                 static_cast<node const*>(it.m_node)->m_data)
                                 );
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator persistent_set<T, U>::find(value_type const& value) const
{
    node_base const* ptr = static_cast<node_base const*>(::bezborodov::find<T, U>(_fake_root.m_left, value).get());
    return { &_fake_root, ptr ? ptr : &_fake_root };
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::value_type const& persistent_set<T, U>::iterator::operator*() const noexcept
{
    return static_cast<node const*>(m_node)->m_data;
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator& persistent_set<T, U>::iterator::operator++() noexcept
{
    node const* current = _fake_root->m_left.get();
    node_base const* successor = _fake_root;
    while (current != nullptr) {
        if (current->m_data < static_cast<node const*>(m_node)->m_data
            || current->m_data == static_cast<node const*>(m_node)->m_data)
        {
            current = current->m_right.get();
        }
        else
        {
            successor = static_cast<node_base const*>(current);
            current = current->m_left.get();
        }
    }
    m_node = successor;
    return *this;
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator persistent_set<T, U>::iterator::operator++(int) noexcept
{
    iterator it(*this);
    ++*this;
    return it;
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator& persistent_set<T, U>::iterator::operator--() noexcept
{
    node const* current = _fake_root->m_left.get();
    node_base const* successor = nullptr;
    bool is_end = (m_node == _fake_root);
    while (current != nullptr) {
        if (is_end || current->m_data < static_cast<node const*>(m_node)->m_data) {
            successor = static_cast<node_base const*>(current);
            current = current->m_right.get();
        } else {
            current = current->m_left.get();
        }
    }
    m_node = successor;
    return *this;
}

template <typename T, template <typename> class U>
inline typename persistent_set<T, U>::iterator persistent_set<T, U>::iterator::operator--(int) noexcept
{
    iterator it(*this);
    --*this;
    return it;
}

template <typename T, template <typename> class U>
inline bool persistent_set<T, U>::iterator::operator==(iterator const& other) noexcept
{
    return m_node == other.m_node && _fake_root == other._fake_root;
}

template <typename T, template <typename> class U>
inline bool persistent_set<T, U>::iterator::operator!=(iterator const& other) noexcept
{
    return !(*this == other);
}

template <typename T, template <typename> class U>
inline persistent_set<T, U>::iterator::iterator(node_base const* _fake_root, node_base const* m_node) noexcept
    : _fake_root(_fake_root)
    , m_node(m_node)
{
}

template <typename T, template <typename> class U>
template <typename>
inline persistent_set<T, U>::node::node(value_type&& value)
    : m_right(nullptr)
    , m_data(std::move(value))
    , m_prior(generator())
{
}

template <typename T, template <typename> class U>
inline persistent_set<T, U>::node::node(value_type const& value)
    : m_right(nullptr)
    , m_data(value)
    , m_prior(generator())
{
}
}
