#include <ctime>
#include <cstdio>
#include <iostream>
#include <type_traits>

#include "persistent_set.h"
#include "gtest/gtest.h"

using namespace bezborodov::smart_ptrs;
using namespace bezborodov;
using namespace std;

TEST(SHARED_PTR_TESTS, nothrow_check)
{
    EXPECT_TRUE(is_nothrow_default_constructible_v<shared_ptr<int>>);
    EXPECT_TRUE(is_nothrow_copy_constructible_v<shared_ptr<int>>);
    EXPECT_TRUE(is_nothrow_move_constructible_v<shared_ptr<int>>);
    EXPECT_TRUE(is_nothrow_copy_assignable_v<shared_ptr<int>>);
    EXPECT_TRUE(is_nothrow_move_assignable_v<shared_ptr<int>>);
    EXPECT_TRUE(is_nothrow_destructible_v<shared_ptr<int>>);
}

TEST(SHARED_PTR_TESTS, default_ctor_check)
{
    shared_ptr<int> x;
    EXPECT_EQ(x.get(), nullptr);
}

TEST(SHARED_PTR_TESTS, move_ctor_check)
{
    shared_ptr<int> x(new int{ 5 });
    shared_ptr<int> y(std::move(x));
    EXPECT_EQ(x.get(), nullptr);
    EXPECT_TRUE(y.get() && *y == 5);
    EXPECT_TRUE(y.count() == 1);
}

TEST(SHARED_PTR_TESTS, copy_assign_test)
{
    shared_ptr<int> x{ new int{ 5 } };
    shared_ptr<int> y;
    y = x;
    EXPECT_TRUE(x.get() && *x == 5);
    EXPECT_TRUE(y.get() && *y == 5);
    EXPECT_TRUE(x.count() == 2);
}
TEST(SHARED_PTR_TESTS, move_assign_test)
{
    shared_ptr<int> x{ new int{ 5 } };
    shared_ptr<int> y;
    y = std::move(x);
    EXPECT_TRUE(x.get() == nullptr);
    EXPECT_TRUE(y.get() && *y == 5);
    EXPECT_TRUE(y.count() == 1);
}

TEST(SHARED_PTR_TESTS, dtor_test)
{
    shared_ptr<int> x{ new int{ 5 } };
    EXPECT_TRUE(x.get() && *x == 5 && x.count() == 1);
    shared_ptr<int> y[20];
    y[0] = x;
    EXPECT_TRUE(x.get() && *x == 5 && x.count() == 2);
    for (int i = 1; i < 20; ++i) {
        y[i] = y[i - 1];
        *y[i] = i;
        EXPECT_TRUE(x.get() && *x == i && x.count() == i + 2);
    }

    {
        shared_ptr<int> z = x;
        EXPECT_TRUE(x.count() == 22);
    }
    EXPECT_TRUE(x.count() == 21);
}

TEST(SHARED_PTR_TESTS, reference_test)
{
    shared_ptr<string> ptr(new string("test"));
    EXPECT_TRUE(ptr->size() == 4);
}

TEST(SHARED_PTR_TESTS, dereference_test)
{
    shared_ptr<string> ptr(new string("test"));
    EXPECT_TRUE(*ptr == "test");
}

TEST(SHARED_PTR_TESTS, get_test)
{
    int* p = new int{ 5 };
    shared_ptr<int> ptr{ p };
    EXPECT_TRUE(ptr.get() == p);
}

TEST(SHARED_PTR_TESTS, count_test)
{
    int* p = new int{ 5 };
    shared_ptr<int> p1{ p };
    EXPECT_TRUE(p1.get() && *p1 == 5 && p1.count() == 1);
    shared_ptr<int> p2(p1);
    EXPECT_TRUE(p2.get() && *p2 == 5 && p2.count() == 2);
    shared_ptr<int> p3(p2);
    EXPECT_TRUE(p3.get() && *p3 == 5 && p3.count() == 3);
}

TEST(SHARED_PTR_TESTS, reset_test)
{
    int* p = new int{ 30 };
    int* q = new int{ 239 };

    shared_ptr<int> p1{ p };
    shared_ptr<int> p2{ p1 };
    shared_ptr<int> p3;
    p3 = p2;
    EXPECT_TRUE(p1.get() && *p1 == 30 && p1.count() == 3);
    p1.reset(q);
    shared_ptr<int> q1 = p1;
    EXPECT_TRUE(p1.get() && *p1 == 239 && p1.count() == 2);
    EXPECT_TRUE(p2.get() && *p2 == 30 && p2.count() == 2);
}

TEST(SHARED_PTR_TESTS, swap_test)
{
    shared_ptr<int> p{ new int{ 11'193 } }; // bitcoin price on 18.01.2018 3:58
    shared_ptr<int> q{ new int{ 11'261 } }; // bitcoin price on 18.01.2018 4:05

    EXPECT_TRUE(p.get() && *p == 11'193);
    EXPECT_TRUE(q.get() && *q == 11'261);
    p.swap(q);
    EXPECT_TRUE(p.get() && *p == 11'261);
    EXPECT_TRUE(q.get() && *q == 11'193);
}

TEST(SHARED_PTR_TESTS, wtf_test)
{
    shared_ptr<int> p{ new int{ 1 } };
    shared_ptr<int> q;
    EXPECT_TRUE(q.get() == nullptr && p.get() && *p == 1 && p.count() == 1);
    q = q;
    EXPECT_TRUE(q.get() == nullptr && p.get() && *p == 1 && p.count() == 1);
    p = p;
    EXPECT_TRUE(q.get() == nullptr && p.get() && *p == 1 && p.count() == 1);
    q = p;
    EXPECT_TRUE(q.get() && p.get() && *p == 1 && *q == 1 && p.count() == 2);
    p = q;
    EXPECT_TRUE(q.get() && p.get() && *p == 1 && *q == 1 && p.count() == 2);
    *p = 30;
    EXPECT_TRUE(q.get() && p.get() && *p == 30 && *q == 30 && p.count() == 2);
    shared_ptr<int> p1;
    shared_ptr<int> p2(p1);
    shared_ptr<int> p3(p1);
    shared_ptr<int> p4(p2);
    shared_ptr<int> p5(p3);
    shared_ptr<int> p6;
    p6 = p4;
    p6 = p5;
    EXPECT_TRUE(p1.count() == 0);
    EXPECT_TRUE(p2.count() == 0);
    EXPECT_TRUE(p3.count() == 0);
    EXPECT_TRUE(p4.count() == 0);
    EXPECT_TRUE(p5.count() == 0);
    EXPECT_TRUE(p6.count() == 0);
}

/* ****************************************************** */

TEST(LINKED_PTR_TESTS, nothrow_check)
{
    EXPECT_TRUE(is_nothrow_default_constructible_v<linked_ptr<int>>);
    EXPECT_TRUE(is_nothrow_copy_constructible_v<linked_ptr<int>>);
    EXPECT_TRUE(is_nothrow_move_constructible_v<linked_ptr<int>>);
    EXPECT_TRUE(is_nothrow_copy_assignable_v<linked_ptr<int>>);
    EXPECT_TRUE(is_nothrow_move_assignable_v<linked_ptr<int>>);
    EXPECT_TRUE(is_nothrow_destructible_v<linked_ptr<int>>);
}

TEST(LINKED_PTR_TESTS, default_ctor_check)
{
    linked_ptr<int> x;
    EXPECT_TRUE(x.get() == nullptr);
}

TEST(LINKED_PTR_TESTS, move_ctor_check)
{
    linked_ptr<int> x{ new int{ 5 } };
    linked_ptr<int> y{ std::move(x) };
    EXPECT_TRUE(x.get() == nullptr);
    EXPECT_TRUE(y.get() && *y == 5);
    EXPECT_TRUE(y.count() == 1);
}

TEST(LINKED_PTR_TESTS, copy_assign_test)
{
    linked_ptr<int> x{ new int{ 5 } };
    linked_ptr<int> y;
    y = x;
    EXPECT_TRUE(x.get() && *x == 5);
    EXPECT_TRUE(y.get() && *y == 5);
    EXPECT_TRUE(x.count() == 2);
}

TEST(LINKED_PTR_TESTS, move_assign_test)
{
    linked_ptr<int> x{ new int{ 5 } };
    linked_ptr<int> y;
    y = std::move(x);
    EXPECT_TRUE(x.get() == nullptr);
    EXPECT_TRUE(y.get() && *y == 5);
    EXPECT_TRUE(y.count() == 1);
}

TEST(LINKED_PTR_TESTS, dtor_test)
{
    linked_ptr<int> x{ new int{ 5 } };
    EXPECT_TRUE(x.get() && *x == 5 && x.count() == 1);
    linked_ptr<int> y[20];
    y[0] = x;
    EXPECT_TRUE(x.get() && *x == 5 && x.count() == 2);
    for (int i = 1; i < 20; ++i) {
        y[i] = y[i - 1];
        *y[i] = i;
        EXPECT_TRUE(x.get() && *x == i && x.count() == i + 2);
    }

    {
        linked_ptr<int> z = x;
        EXPECT_TRUE(x.count() == 22);
    }
    EXPECT_TRUE(x.count() == 21);
}

TEST(LINKED_PTR_TESTS, reference_test)
{
    linked_ptr<string> ptr{ new string{ "test" } };
    EXPECT_TRUE(ptr->size() == 4);
}

TEST(LINKED_PTR_TESTS, dereference_test)
{
    linked_ptr<string> ptr{ new string{ "test" } };
    EXPECT_TRUE(*ptr == "test");
}

TEST(LINKED_PTR_TESTS, get_test)
{
    int* p = new int{ 5 };
    linked_ptr<int> ptr{ p };
    EXPECT_TRUE(ptr.get() == p);
}

TEST(LINKED_PTR_TESTS, count_test)
{
    int* p = new int{ 5 };
    linked_ptr<int> p1{ p };
    EXPECT_TRUE(p1.get() && *p1 == 5 && p1.count() == 1);
    linked_ptr<int> p2{ p1 };
    EXPECT_TRUE(p2.get() && *p2 == 5 && p2.count() == 2);
    linked_ptr<int> p3{ p2 };
    EXPECT_TRUE(p3.get() && *p3 == 5 && p3.count() == 3);
}

TEST(LINKED_PTR_TESTS, reset_test)
{
    int* p = new int{ 30 };
    int* q = new int{ 239 };

    linked_ptr<int> p1{ p };
    linked_ptr<int> p2{ p1 };
    linked_ptr<int> p3;
    p3 = p2;
    EXPECT_TRUE(p1.get() && *p1 == 30 && p1.count() == 3);
    p1.reset(q);
    linked_ptr<int> q1 = p1;
    EXPECT_TRUE(p1.get() && *p1 == 239 && p1.count() == 2);
    EXPECT_TRUE(p2.get() && *p2 == 30 && p2.count() == 2);
}

TEST(LINKED_PTR_TESTS, swap_test)
{
    linked_ptr<int> p{ new int{ 11'193 } };
    linked_ptr<int> q{ new int{ 11'261 } };

    EXPECT_TRUE(p.get() && *p == 11'193);
    EXPECT_TRUE(q.get() && *q == 11'261);
    p.swap(q);
    EXPECT_TRUE(p.get() && *p == 11'261);
    EXPECT_TRUE(q.get() && *q == 11'193);
}

TEST(LINKED_PTR_TESTS, wtf_test)
{
    linked_ptr<int> p{ new int{ 1 } };
    linked_ptr<int> q;
    EXPECT_TRUE(q.get() == nullptr && p.get() && *p == 1 && p.count() == 1);
    q = q;
    EXPECT_TRUE(q.get() == nullptr && p.get() && *p == 1 && p.count() == 1);
    p = p;
    EXPECT_TRUE(q.get() == nullptr && p.get() && *p == 1 && p.count() == 1);
    q = p;
    EXPECT_TRUE(q.get() && p.get() && *p == 1 && *q == 1 && p.count() == 2);
    p = q;
    EXPECT_TRUE(q.get() && p.get() && *p == 1 && *q == 1 && p.count() == 2);
    *p = 30;
    EXPECT_TRUE(q.get() && p.get() && *p == 30 && *q == 30 && p.count() == 2);
    linked_ptr<int> p1;
    linked_ptr<int> p2(p1);
    linked_ptr<int> p3(p1);
    linked_ptr<int> p4(p2);
    linked_ptr<int> p5(p3);
    linked_ptr<int> p6;
    p6 = p4;
    p6 = p5;
    EXPECT_TRUE(p1.count() == 0);
    EXPECT_TRUE(p2.count() == 0);
    EXPECT_TRUE(p3.count() == 0);
    EXPECT_TRUE(p4.count() == 0);
    EXPECT_TRUE(p5.count() == 0);
    EXPECT_TRUE(p6.count() == 0);
}

/* ****************************************************** */

template <typename T>
vector<T> vec(persistent_set<T> const& s)
{
    vector<T> res;
    for (auto it = s.cbegin(); it != s.cend(); ++it) {
        res.push_back(*it);
    }
    return res;
}

TEST(PERSISTENT_SET_TESTS, insert)
{
    persistent_set<int> s;
    s.insert(32);
    s.insert(33);
    EXPECT_EQ(vector<int>({ 32, 33 }), vec(s));
}

TEST(PERSISTENT_SET_TESTS, insert_10_elements)
{
    persistent_set<int> s;
    for (int i = 0; i < 5; ++i) {
        s.insert(i);
    }
    for (int j = 9; j >= 5; --j) {
        s.insert(j);
    }
    EXPECT_EQ(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }), vec(s));
}

TEST(PERSISTENT_SET_TESTS, copy)
{
    persistent_set<int> s;
    s.insert(1);
    persistent_set<int> p(s);
    p.insert(2);
    EXPECT_EQ(vector<int>({ 1, 2 }), vec(p));
    EXPECT_EQ(vector<int>({ 1 }), vec(s));
}

TEST(PERSISTENT_SET_TESTS, copy2)
{
    persistent_set<int> s;
    s.insert(12);
    persistent_set<int> p(s);
    s.insert(45);
    EXPECT_EQ(vector<int>({ 12, 45 }), vec(s));
    EXPECT_EQ(vector<int>({ 12 }), vec(p));
    p.insert(0);
    EXPECT_EQ(vector<int>({ 12, 45 }), vec(s));
    EXPECT_EQ(vector<int>({ 0, 12 }), vec(p));
}

TEST(PERSISTENT_SET_TESTS, inset_and_copy)
{
    persistent_set<int> s;
    s.insert(4);
    s.insert(5);
    s.insert(7);
    s.insert(6);
    s.insert(1);
    s.insert(2);
    s.insert(8);
    s.insert(3);
    s.insert(0);
    s.insert(9);
    EXPECT_EQ(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }), vec(s));
    persistent_set<int> p(s);
    s.insert(100);
    EXPECT_EQ(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 100 }), vec(s));
    EXPECT_EQ(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }), vec(p));
}

TEST(PERSISTENT_SET_TESTS, erase0)
{
    persistent_set<int> s;
    s.insert(4);
    s.erase(s.begin());
    EXPECT_EQ(vector<int>({}), vec(s));
}

TEST(PERSISTENT_SET_TESTS, erase)
{
    persistent_set<int> s;
    EXPECT_EQ(vector<int>({}), vec(s));
    
    s.insert(4);
    EXPECT_EQ(vector<int>({ 4 }), vec(s));
    
    s.insert(5);
    EXPECT_EQ(vector<int>({ 4, 5 }), vec(s));
    
    s.insert(7);
    EXPECT_EQ(vector<int>({ 4, 5, 7 }), vec(s));
    
    s.insert(6);
    EXPECT_EQ(vector<int>({ 4, 5, 6, 7 }), vec(s));
    
    s.insert(1);
    EXPECT_EQ(vector<int>({ 1, 4, 5, 6, 7 }), vec(s));
    
    s.insert(2);
    EXPECT_EQ(vector<int>({ 1, 2, 4, 5, 6, 7 }), vec(s));
    
    s.insert(8);
    EXPECT_EQ(vector<int>({ 1, 2, 4, 5, 6, 7, 8 }), vec(s));
    
    s.insert(3);
    EXPECT_EQ(vector<int>({ 1, 2, 3, 4, 5, 6, 7, 8 }), vec(s));
    
    s.insert(0);
    EXPECT_EQ(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7, 8 }), vec(s));
    
    s.insert(9);
    EXPECT_EQ(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }), vec(s));

    s.erase(s.begin());
    EXPECT_EQ(vector<int>({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }), vec(s));

    s.erase(s.find(5));
    EXPECT_EQ(vector<int>({ 1, 2, 3, 4, 6, 7, 8, 9 }), vec(s));

    s.erase(s.find(3));
    EXPECT_EQ(vector<int>({ 1, 2, 4, 6, 7, 8, 9 }), vec(s));

    s.erase(s.find(7));
    EXPECT_EQ(vector<int>({ 1, 2, 4, 6, 8, 9 }), vec(s));

    s.erase(s.find(1));
    EXPECT_EQ(vector<int>({ 2, 4, 6, 8, 9 }), vec(s));

    s.erase(s.find(8));
    EXPECT_EQ(vector<int>({ 2, 4, 6, 9 }), vec(s));

    s.erase(s.find(9));
    EXPECT_EQ(vector<int>({ 2, 4, 6 }), vec(s));

    s.erase(s.find(4));
    EXPECT_EQ(vector<int>({ 2, 6 }), vec(s));

    s.erase(s.begin());
    EXPECT_EQ(vector<int>({ 6 }), vec(s));

    s.erase(s.begin());
    EXPECT_EQ(vector<int>({}), vec(s));

    s.insert(100);
    EXPECT_EQ(vector<int>({ 100 }), vec(s));

    s.erase(s.find(100));
    EXPECT_EQ(vector<int>({}), vec(s));
}

TEST(PERSISTENT_SET_TESTS, erase2)
{
    persistent_set<int> s;
    s.insert(4);
    s.insert(5);
    s.insert(7);
    s.insert(6);
    s.insert(1);
    s.insert(2);
    s.insert(8);
    s.insert(3);
    s.insert(0);
    s.insert(9);
    EXPECT_EQ(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }), vec(s));
    persistent_set<int> p;
    for (int i = 0; i < 10; i++) {
        p = s;
        auto it = p.begin();
        for (int j = 0; j < i; j++) {
            it++;
        }
        p.erase(it);
        vector<int> ans({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
        ans.erase(ans.begin() + i);
        EXPECT_EQ(ans, vec(p));
    }
}

struct smth {
    int var;
    smth(int var) : var(var) {}
    smth() = delete;
    smth(smth const& rhs) {
        var = rhs.var;
    }
    smth(smth&& rhs) noexcept {
        var = rhs.var;
    }
    bool operator<(smth const& rhs) const {
        return var < rhs.var;
    }
    bool operator==(smth const& rhs) const {
        return var == rhs.var;
    }
};

TEST(PERSISTENT_SET_TESTS, smth)
{
    persistent_set<smth> s;
    s.insert(smth(2));
    s.insert(smth(1));
    s.erase(s.find(2));
    s.insert(smth(5));
    EXPECT_EQ(vector<smth>({1, 5}), vec(s));
    persistent_set<smth> p(s);
    p.insert(smth(0));
    EXPECT_EQ(vector<smth>({ 1, 5 }), vec(s));
    EXPECT_EQ(vector<smth>({ 0, 1, 5 }), vec(p));
    persistent_set<smth> q(s);
    s.insert(smth(8));
    EXPECT_EQ(vector<smth>({ 1, 5 }), vec(q));
    EXPECT_EQ(vector<smth>({ 1, 5, 8 }), vec(s));
    q = s;
    EXPECT_EQ(vector<smth>({ 1, 5, 8 }), vec(s));
    EXPECT_EQ(vector<smth>({ 1, 5, 8 }), vec(q));
    s.erase(s.find(8));
    EXPECT_EQ(vector<smth>({ 1, 5 }), vec(s));
    EXPECT_EQ(vector<smth>({ 1, 5, 8 }), vec(q));
}

TEST(PERSISTENT_SET_TESTS, decrease_end) {
    persistent_set<int> s;
    s.insert(4);
    s.insert(12);
    s.insert(1);
    persistent_set<int>::iterator it = s.end();
    it--;
    EXPECT_EQ(12, *it);
}

template < typename Set >
std::vector < std::vector < int > > test(std::string const& msg) {
    int tt = clock();
    srand(100);
    std::vector < Set > ss(256);
    for (int i = 0; i < 1000000; ++i) {
        //if (i % 100000 == 0)
            //std::cout << i << std::endl;
        int r = rand() & 7;
        if (r == 0) {
            int j = rand() & 255;
            ss[j] = ss[rand() & 255];
        } else if (r <= 3) {
            int j = rand() & 255;
            ss[j].insert(rand() & 16383);
        } else {
            int j = rand() & 255;
            auto it = ss[j].find(rand() & 16383);
            if (it != ss[j].end())
                ss[j].erase(it);
        }
    }
    std::vector < std::vector < int > > a(256);
    for (int i = 0; i < 256; ++i)
        for (int x : ss[i])
            a[i].push_back(x);
    std::cout << msg << " " << 1.*(clock() - tt) / CLOCKS_PER_SEC << "\n";
    return a;
}

TEST(PERSISTENT_SET_TESTS, random)
{
    auto r1 = test<persistent_set<int, shared_ptr>>("shared_ptr");
    auto r2 = test<persistent_set<int, linked_ptr>>("linked_ptr");
    EXPECT_EQ(r1, r2);
    auto r3 = test<set<int>>("std::set  ");
    EXPECT_EQ(r2, r3);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    std::set<int> q;
    return RUN_ALL_TESTS();
}
